import React from 'react';
import { Provider } from 'react-redux';
import { StyleSheet, Text, View, Picker } from 'react-native';
import HomeContainer from './app/containers/HomeContainer';
import { configureStore } from './app/store';

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.store = configureStore();
  }

  render() {
    return (
      <Provider store={this.store}>
        <View style={styles.container}>
          <HomeContainer />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
});
