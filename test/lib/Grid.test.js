import _ from 'lodash';

import Grid from '../../app/util/Grid';
import Bubble from '../../app/util/Bubble';

const trends = [
  {
    name: '2nd most twd',
    volume: 859,
  },
  {
    name: 'Most twd',
    volume: 1000,
  },
  {
    name: 'bad twd',
    volume: 2,
  },
  {
    name: 'less twd',
    volume: 55,
  },
]

it('instantiates without crashing', () => {
  const grid = new Grid(300, 500);
  expect(grid.bubbles.length).toBe(0);
});

it('adds bubbles sorted by radius', () => {
  const grid = new Grid(300, 500);
  grid.setTrends(trends)

  expect(_.map(grid.bubbles, 'radius')).toEqual([50, 45, 19, 18]);
});

it('adds coordinates to all bubbles', () => {
  const grid = new Grid(300, 500);
  grid.setTrends(trends)

  grid.bubbles.map((bubble) => {
    expect(bubble.hasCoordinates()).toBeTruthy();
  })
});
