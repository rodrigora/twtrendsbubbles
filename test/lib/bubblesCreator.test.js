
import { createTrendsBubbles } from '../../app/util/bubblesCreator';

 const bubbles = createTrendsBubbles(300, 500, [
    {
      name: '2nd most twd',
      volume: 859,
    },
    {
      name: 'Most twd',
      volume: 1000,
    },
    {
      name: 'less twd',
      volume: 59,
    },
    {
      name: 'no tweets',
      volume: 0,
    }
  ]);

it('creates bubbles with proportional size', () => {
  expect(bubbles.length).toBe(4);

  expect(bubbles[0].trend.name).toEqual('Most twd');
  expect(bubbles[1].trend.name).toEqual('2nd most twd');
  expect(bubbles[2].trend.name).toEqual('less twd');
  expect(bubbles[3].trend.name).toEqual('no tweets');

  expect(bubbles[0].radius > bubbles[1].radius).toBeTruthy();
  expect(bubbles[1].radius > bubbles[2].radius).toBeTruthy();
  expect(bubbles[2].radius > bubbles[3].radius).toBeTruthy();
});

it('the biggest bubble radius should be 1/6 of the xAxis size', () => {
  expect(bubbles[0].radius).toBe(50);
});

it('the smallest bubble radius should be 1/16 of the xAxis size (rounded)', () => {
  expect(bubbles[3].radius).toBe(18);
});
