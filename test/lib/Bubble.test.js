import _ from 'lodash';

import Bubble from '../../app/util/Bubble';


it('instantiates without crashing', () => {
  new Bubble('label', 10); //without position
  new Bubble('label', 10, 0, 0);
});

it('checks if collides with another bubble', () => {
  const bubble = new Bubble('label', 10, 0, 0);

  expect(bubble.collides(new Bubble('label', 10, 0, 0))).toBeTruthy();
  expect(bubble.collides(new Bubble('label', 10, 10, 10))).toBeTruthy();

  expect(bubble.collides(new Bubble('label', 10, 20, 20))).toBeFalsy();
  expect(bubble.collides(new Bubble('label', 10, 20, 1))).toBeFalsy();

  expect((new Bubble('', 93, 135, 395)).collides(new Bubble('', 68, 153, 466))).toBeTruthy();
});

it('never collides with an bubble without coordinates', () => {
  expect((new Bubble('', 10)).collides(new Bubble('', 10, 0, 0))).toBeFalsy();
  expect((new Bubble('', 10, 0, 0)).collides(new Bubble('', 10))).toBeFalsy();
});

it('doesnt collides with itself', () => {
  const bubble = new Bubble('bubble', 10, 0, 0);

  expect(bubble.collides(bubble)).toBeFalsy();
});

it('checks if it has coordinates', () => {
  const bubble = new Bubble('bubble', 10);
  expect(bubble.hasCoordinates()).toBeFalsy();

  bubble.setCoordinates(0,0);
  expect(bubble.hasCoordinates()).toBeTruthy();
});

it('clear coordinates', () => {
  const bubble = new Bubble('bubble', 10, 10, 10);
  expect(bubble.hasCoordinates()).toBeTruthy();
  bubble.clearCoordinates();
  expect(bubble.hasCoordinates()).toBeFalsy();
});

