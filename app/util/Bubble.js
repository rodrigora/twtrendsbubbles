import _ from 'lodash';

export default class Bubble {
  constructor (trend, radius, x, y) {
    this.trend = trend;
    this.radius = radius;
    this.x = x;
    this.y = y;
  }

  setCoordinates (x, y) {
    this.x = x;
    this.y = y;
  }

  clearCoordinates () {
    this.x = null;
    this.y = null;
  }

  collides (other, idx) {
    if (this === other) {
      return false;
    }
    if (!this.hasCoordinates() || !other.hasCoordinates()) {
      return false;
    }
    const dx = this.x - other.x;
    const dy = this.y - other.y;
    const rr = this.radius + other.radius;
    return Math.sqrt((dx * dx) + (dy * dy)) <= rr;
  }

  hasCoordinates () {
    return _.isInteger(this.x) && _.isInteger(this.y);
  }

  print () {
    console.log(this.toString())
  }

  toString() {
    return `${this.trend.name}(${this.x},${this.y},${this.radius})`;
  }
}
