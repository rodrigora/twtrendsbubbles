import _ from 'lodash';
import Bubble from './Bubble';

export function createTrendsBubbles (xAxis, yAxis, trends) {
  const maxRadius = _.parseInt(xAxis / 6);
  const minRadius = _.parseInt(xAxis / 16);
  const radiusDiff = maxRadius - minRadius;
  const sortedTrends = _.reverse(_.sortBy(trends, 'volume'));
  const mostTwitted = _.head(sortedTrends).volume;

  return sortedTrends.map((trend) => {
    const radiusInc = _.parseInt((trend.volume * radiusDiff)/mostTwitted);
    return new Bubble(trend, minRadius + radiusInc);
  })
}
