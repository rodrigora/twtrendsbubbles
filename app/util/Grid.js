import _ from 'lodash';
import { createTrendsBubbles } from './bubblesCreator'

export default class Grid {
  constructor (xAxis, yAxis) {
    this.xAxis = xAxis;
    this.yAxis = yAxis;
    this.bubbles = [];
  }

  setTrends (trends) {
    this.bubbles = createTrendsBubbles(this.xAxis, this.yAxis, trends);
    this.placeBubbles();
  }

  placeBubbles () {
    let currentBubbleIdx = 0;
    let interactions = 0;

    while (this.bubbles[currentBubbleIdx]) {
      interactions += this.placeBubble(this.bubbles[currentBubbleIdx]);
      currentBubbleIdx++;
    }
  }

  placeBubble (bubble) {
    let interactions = 0;
    this.setRandomCoodinates(bubble);

    while (!this.canAdd(bubble) && interactions < 200) {
      this.setRandomCoodinates(bubble);
      interactions ++;
    }

    if(interactions === 200) {
      // couldn't find a place for bubble
      bubble.clearCoordinates();
    }

    return interactions + 1;
  }

  // gerenates random coordinates for the bubble avoid collisions with the grid
  // axes
  setRandomCoodinates(bubble) {
    const x = _.random(bubble.radius, this.xAxis - bubble.radius);
    const y = _.random(bubble.radius, this.yAxis - bubble.radius);
    bubble.setCoordinates(x, y);
  }

  // check if the bubble collides with any bubble placed in the grid
  canAdd (bubble) {
    const collision = _.find(this.bubbles, (gridBubble) => {
      return gridBubble.collides(bubble)
    });

    return _.isEmpty(collision);
  }

  // for debugging
  print () {
    this.bubbles.forEach((bubble) => {
      bubble.print();
    })
  }

}
