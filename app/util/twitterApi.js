import twitterTrends from './twitterTrends';
import _ from 'lodash';

const generateResponse = () => {
  let trends = _.take(_.shuffle(twitterTrends), 20)
  trends = trends.map((trend) => ({
    name: trend,
    volume: _.random(1000000),
  }))

  return {
    json: () => ({trends})
  };
}

export const fetchTwitter = () => {
  return new Promise(resolve => resolve(generateResponse()));
}
