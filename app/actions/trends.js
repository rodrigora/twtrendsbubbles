import { fetchTwitter } from '../util/twitterApi';
export const REQUEST_TRENDS = 'REQUEST_TRENDS';
export const RECEIVE_TRENDS = 'RECEIVE_TRENDS';

export function fetchTrends () {
  return (dispatch) => {
    dispatch(requestTrends())

    //GET https://api.twitter.com/1.1/trends/place.json?id=19
    setTimeout(() => {
      return fetchTwitter('https://api.twitter.com/1.1/trends/place.json?id=19')
      .then((response) => response.json())
      .then((responseJson) => {
        dispatch(receiveTrends(responseJson.trends)) ;
      })
      .catch((error) => {
        console.error(error);
      })
    }, 1000);

  }
}

const requestTrends = () => ({
  type: REQUEST_TRENDS
})

const receiveTrends = (trends) => ({
  type: RECEIVE_TRENDS,
  trends
})
