import {REQUEST_TRENDS, RECEIVE_TRENDS} from '../actions/trends';
import _ from 'lodash';

const initialState = {
  list: [],
  totalVolume: 0,
};

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case REQUEST_TRENDS:
      return initialState;
    case RECEIVE_TRENDS:
      return {
        list: _.sortBy(action.trends, 'volume'),
        totalVolume: _.sum(_.map(action.trends, 'volume'))
      };
    default:
      return state;
  }
}

export default reducer
