import { combineReducers } from 'redux';
import trends from './trends';
import loading from './loading';

export default combineReducers({
  loading,
  trends,
})
