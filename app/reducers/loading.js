import {REQUEST_TRENDS, RECEIVE_TRENDS} from '../actions/trends';

const initialState = false;

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case REQUEST_TRENDS:
      return true;
    case RECEIVE_TRENDS:
      return false;
    default:
      return state;
  }
}

export default reducer
