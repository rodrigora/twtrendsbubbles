import { connect } from 'react-redux'
import {fetchTrends} from '../actions/trends'
import Home from '../components/Home';

const select = (state) => state

const actions = {
  fetchTrends
}

export default connect(select, actions)(Home)
