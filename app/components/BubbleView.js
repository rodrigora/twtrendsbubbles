import React, {PropTypes, Component} from 'react';
import { View, Text, ScrollView, Alert, TouchableHighlight } from 'react-native';

class BubbleView extends Component {
  static propTypes = {
  }

  constructor (props) {
    super(props);

    this.onPress = this.onPress.bind(this);
  }

  onPress () {
    const { bubble } = this.props;
    Alert.alert(
      `Trending: ${bubble.trend.name}`,
      `${bubble.trend.volume} tweets`,
      [
        {text: 'OK', onPress: () => console.log('OK Pressed')},
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
      ],
      { cancelable: true }
    )
  }

  render () {
    const { bubble } = this.props;

    if (!bubble.hasCoordinates()) {
      return null;
    }

    const diameter = bubble.radius * 2;
    const centerY = bubble.y - bubble.radius;
    const centerX = bubble.x - bubble.radius;

    return (
      <TouchableHighlight
        onPress={this.onPress}>
        <View style={{
          position: 'absolute',
          top: centerY,
          left: centerX,
          width: diameter,
          height: diameter,
          borderRadius: bubble.radius,
          backgroundColor: '#77cfc3',
          alignItems: 'center',
          justifyContent: 'center',
          padding: 5,
          overflow: 'hidden',
        }}>
          <Text style={{color: 'white'}}>{bubble.trend.name}</Text>
        </View>
      </TouchableHighlight>
    );
  }
}

export default BubbleView;
