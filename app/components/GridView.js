import React, {Component} from 'react';
import { View, Text, ScrollView } from 'react-native';
import BubbleView from './BubbleView';

// Grid view with some bubbles
export default class GridView extends Component {

  render () {
    const { grid } = this.props;

    if (!grid) {
      return null;
    }

    return (
      <View style={{width: grid.xAxis, height: grid.yAxis}}>
        {grid.bubbles.map((bubble, key) => {
          return <BubbleView key={key} bubble={bubble} idx={key} />
        })}
      </View>
    );
  }
}
