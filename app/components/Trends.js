import React, {PropTypes, Component} from 'react';
import { StyleSheet, View, Text, ScrollView } from 'react-native';
import { Header, Icon } from 'react-native-elements';
import GridView from './GridView';
import Grid from '../util/Grid';
import _ from 'lodash';

// View with two grids of ten bubbles each.
// Each Grid has the Screen size
export default class Trends extends Component {
  static propTypes = {
  }

  constructor (props) {
    super(props);

    this.state = {
      width: 0,
      height: 0,
    }

    this.setDimensions = this.setDimensions.bind(this);
  }

  // reads the device's screen size
  setDimensions (event) {
    const {width, height} = event.nativeEvent.layout;
    if (this.state.width != width) {
      this.setState({width, height});
    }
  }

  componentWillUpdate(nextProps, nextState) {
    const {width, height} = nextState;
    this.buildGrids(width, height, nextProps.list);
  }

  // builds the two grids with 10 bubbles each
  // the logic of placing bubbles is handled by Grid class
  buildGrids (screenWidth, screenHeight, trendsList) {
    this.firstGrid = new Grid(screenWidth, screenHeight);
    this.sndGrid = new Grid(screenWidth, screenHeight);

    if (screenWidth > 0 && screenHeight > 0 && !_.isEmpty(trendsList)) {
      const shuffledList = _.shuffle(trendsList);

      this.firstGrid.setTrends(_.take(shuffledList, 10));
      this.sndGrid.setTrends(_.takeRight(shuffledList, 10));
    }
  }

  render () {
    if (this.props.loading || _.isEmpty(this.props.list)) {
      return null;
    }

    return (
      <View style={styles.list} onLayout={this.setDimensions}>
        <ScrollView>
          <GridView grid={this.firstGrid}></GridView>
          <GridView grid={this.sndGrid}></GridView>
        </ScrollView>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  list: {
    flex: 1,
    backgroundColor: '#313133',
  }
});
