import React from 'react';
import { StyleSheet, View, ActivityIndicator } from 'react-native';

// Fills a View with a centered loading spinner
const LoadingArea = ({loading}) => {
  if (!loading) {
    return null;
  }

  return (
    <View style={styles.loadingArea}>
      <ActivityIndicator animating size='large'/>
    </View>
  );
}


const styles = StyleSheet.create({
  loadingArea: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default LoadingArea;
