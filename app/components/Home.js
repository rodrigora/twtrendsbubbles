import React from 'react';
import { StyleSheet, View, Text, ScrollView } from 'react-native';
import { Header, Icon, ListItem, List } from 'react-native-elements';
import LoadingArea from './LoadingArea';
import Trends from './Trends';

// Home Screen with 2 states:
// - Loading
// - Showind Trends
export default class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount () {
    this.props.fetchTrends();
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerWrapper}>
          <Header
            backgroundColor="#03a9f4"
            centerComponent={{ text: 'Worldwide trends', style: { color: '#fff' }}}
            rightComponent={<Icon name="autorenew" color='#fff' onPress={this.props.fetchTrends} />}
          />

        </View>
        <LoadingArea loading={this.props.loading} />
        <Trends loading={this.props.loading} list={this.props.trends.list} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerWrapper: {
    height: 70,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#03a9f4',
  },
  list: {
    flex: 1,
    backgroundColor: '#fff',
  }
});
